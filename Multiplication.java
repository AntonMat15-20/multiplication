package ru.maa.multiplacation;

import java.util.Scanner;

/**
 * Класс для умножение 2 множителей без умножения
 */

public class Multiplication {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите 1 множитель = ");
        int a = scan.nextInt();
        System.out.print("Введите 2 множитель = ");
        int b = scan.nextInt();

        System.out.println("Произведение = " + multiplication(a, b));

    }
    public static int multiplication(int firstMultiplier, int secondMultiplier) {
        int result = 0;

        if (firstMultiplier == 0 || secondMultiplier == 0) {
            return 0;
        }

        if (firstMultiplier < 0 && secondMultiplier < 0) {
            firstMultiplier = -firstMultiplier;
            secondMultiplier = -secondMultiplier;
        }

        if (firstMultiplier > secondMultiplier) {
            int p;
            p = secondMultiplier;
            secondMultiplier = firstMultiplier;
            firstMultiplier = p;
        }

        for (int i = 0; i < secondMultiplier; i++) {
            result += firstMultiplier;
        }
        return result;
    }
}